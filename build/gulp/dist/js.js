'use strict';

var commander = require('../../lib/commander');
var debug = require('../../lib/debug');
var expect = require('../../lib/expect');
var fs = require('fs-extra');
var galvatron = require('../../lib/galvatron');
var gulp = require('gulp');
var gulpRename = require('gulp-rename');
var mac = require('mac');
var minify = require('../../lib/minify');

module.exports = mac.parallel(function () {
    var bundle = galvatron.bundle('src/js/aui*.js', {
        common: '**/aui.js'
    });

    fs.removeSync('dist/aui/js');
    fs.mkdirsSync('dist/aui/js');
    fs.mkdirsSync('.tmp/dist/aui/js');

    return gulp.src(bundle.files)
        .pipe(bundle.watchIf(commander.watch))
        .pipe(debug({title: 'dist/js/compile'}))
        .pipe(bundle.stream())
        .pipe(gulp.dest('dist/aui/js'))
        .pipe(gulp.dest('.tmp/dist/aui/js'))
        .pipe(debug({title: 'dist/js/optimise'}))
        .pipe(minify())
        .pipe(gulpRename({suffix: '.min'}))
        .pipe(gulp.dest('dist/aui/js'))
        .pipe(gulp.dest('.tmp/dist/aui/js'))
        .pipe(expect(bundle.files));
});
