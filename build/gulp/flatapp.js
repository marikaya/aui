'use strict';

var dist = require('./dist');
var flatappServer = require('./flatapp/server');
var flatappCopy = require('./flatapp/copy');
var flatappBuild = require('./flatapp/build');
var i18n = require('./i18n');
var mac = require('mac');

module.exports = mac.series(
    dist,
    i18n,
    flatappCopy,
    flatappBuild,
    flatappServer
);
