'use strict';

var del = require('del');

module.exports = function (done) {
    del('.tmp/docs', done);
};
