'use strict';

var commander = require('./commander');
var debug = require('./debug');
var expect = require('./expect');
var gulp = require('gulp');
var gulpWatch = require('gulp-watch');
var streamIf = require('./stream-if');

module.exports = function (src, dest) {
    return function () {
        return gulp.src(src)
            .pipe(streamIf(commander.watch, gulpWatch.bind(null, src)))
            .pipe(debug({title: 'copy and watch'}))
            .pipe(gulp.dest(dest))
            .pipe(expect(src));
    };
};
