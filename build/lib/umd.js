var babel = require('gulp-babel');
var combiner = require('stream-combiner2');
var filter = require('gulp-filter');
var gulp = require('gulp');
var mac = require('mac');
var mapStream = require('map-stream');
var sourcemaps = require('gulp-sourcemaps');
var vinylTransform = require('vinyl-transform');
var versionReplace = require('./version-replace');

var gulpVersionReplace = vinylTransform(function () {
    return mapStream(function (data, next) {
        return next(null, versionReplace(data));
    });
});

var babelOptions = {
    modules: 'umd'
};
var toBabel = filter(['js/aui/**', 'js/aui*.js']);

module.exports.sources = [
    'src/{js,js-vendor}/**',
    'src/js/aui-soy.js'
];

module.exports.pipeline = function () {
    return combiner(
        gulpVersionReplace,
        toBabel,
        sourcemaps.init(),
        babel(babelOptions),
        sourcemaps.write('.'),
        toBabel.restore()
    );
};
