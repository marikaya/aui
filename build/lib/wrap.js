module.exports = function (before, after) {
    return function (str) {
        return before + str + after;
    }
}
