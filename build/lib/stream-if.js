'use strict';

var through = require('through');

module.exports = function (condition, callback, otherwise) {
    if (condition) {
        return typeof callback === 'function' ? callback() : callback;
    }
    return typeof otherwise === 'function' ? otherwise() : otherwise || through();
};
