'use strict';

var gulp = require('gulp');
var gulpTap = require('gulp-tap');

module.exports = function (total) {
    var files = [];
    var processed = [];
    var done = false;
    var stream = gulpTap(function (file) {
        if (done) {
            return;
        }

        file = file.path;

        if (processed.indexOf(file) === -1) {
            processed.push(file);
        }

        if (files.length === processed.length) {
            done = true;
            stream.emit('continue');
        }
    });

    // Gather a list of files we need to process.
    gulp.src(total).pipe(gulpTap(function (file) {
        files.push(file.path);
    }));

    return stream;
};
