// TODO When Skate 0.14+ ships this will be default import of a camel-case file.
import { camelCase } from 'skatejs/lib/utils';
import element from '../element';
import skate from 'skatejs';

export default function (name, definition) {
    return element[camelCase(name)] = skate(`aui-${name}`, definition);
}
