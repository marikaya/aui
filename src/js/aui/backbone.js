import _ from './underscore';
import Backbone from 'backbone';

if (!window.Backbone) {
    window.Backbone = Backbone;
}

export default window.Backbone;
