'use strict';

import $ from './jquery';
import '../../js-vendor/jquery/jquery.tipsy';

$.fn.tooltip = function (options) {
    return this.each(function(){
        var $this = $(this);
        var allOptions = $.extend({}, $.fn.tooltip.defaults, options);

        // Pass string values straight to tipsy
        if (typeof options === 'string') {
            $this.tipsy(options);

            if(options === 'destroy'){
                if(allOptions.live){
                    $($this.context).off('.tipsy', $this.selector);
                } else {
                    $this.unbind('.tipsy');
                }
            }
            return;
        }

        $this.tipsy(allOptions);

        if (allOptions.hideOnClick && (allOptions.trigger === 'hover' || !allOptions.trigger && $this.tipsy.defaults.trigger === 'hover')) {
            var onClick = function() {
                $this.tipsy('hide');
            };

            if (allOptions.live) {
                $($this.context).on('click.tipsy', this.selector, onClick);
            } else {
                $this.bind('click.tipsy', onClick);
            }
        }
    });
};

$.fn.tooltip.defaults = {
    opacity: 1.0,
    offset: 1,
    delayIn: 500,
    hoverable: true,
    hideOnClick: true,
    aria: true
};
