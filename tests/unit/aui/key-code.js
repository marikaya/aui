'use strict';

import keyCode from '../../../src/js/aui/key-code';

describe('aui/key-code', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            keyCode: keyCode
        });
    });
});
