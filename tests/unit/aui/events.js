'use strict';

import * as events from '../../../src/js/aui/events';

describe('aui/events', function () {
    it('global', function () {
        expect(AJS).to.contain(events);
    });
});
