'use strict';

import '../../../src/js/aui/tooltip';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';

describe('aui/tooltip', function () {
    var clock;
    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    function visibleTipsies () {
        return $(document.body).find('.tipsy').filter(':visible');
    }

    describe('on a <div>', function () {
        var $el;
        var TIPSY_DELAY = 10;

        beforeEach(function () {
            $el = $('<div title="my element">Element</div>');
            $(document.body).append($el);
            $el.tooltip({
                delayIn: TIPSY_DELAY
            });
        });

        afterEach(function () {
            clock.restore();
            $el.remove();
            $(document.body).find('.tipsy').remove();
        });

        it('will show up when delayed', function () {
            expect(visibleTipsies().length).to.equal(0);

            helpers.hover($el);

            clock.tick(1);
            expect(visibleTipsies().length).to.equal(0);

            clock.tick(TIPSY_DELAY);
            var $tipsies = visibleTipsies();
            expect($tipsies.length).to.equal(1);

            var position = $tipsies.position();
            expect(position.left).to.not.equal(0);
            expect(position.top).to.not.equal(0);
        });

        it('will not show when they are delayed and if the underlying element is gone', function () {
            expect(visibleTipsies().length).to.equal(0);

            helpers.hover($el);

            $el.remove();

            clock.tick(TIPSY_DELAY);
            expect(visibleTipsies().length).to.equal(0);
            expect($('.tipsy').length).to.not.be.defined;
        });

        it('trigger has aria-describedby should be on the link with correct id', function () {
            helpers.hover($el);
            clock.tick(TIPSY_DELAY);
            var tipsyId = visibleTipsies()[0].id;
            expect($el.attr('aria-describedby')).to.equal(tipsyId);
        });

        it('tooltip should have an id', function () {
            helpers.hover($el);
            clock.tick(TIPSY_DELAY);
            var tipsyId = visibleTipsies()[0].id;
            expect(visibleTipsies()[0].id).to.not.equal('');
        });
    });

    describe('on svgs', function () {
        var $el;
        var $rect;

        var TIPSY_MARGIN = 50;

        beforeEach(function () {
            clock = sinon.useFakeTimers();
            var svgHtml = '<svg width="300" height="300">' +
                '<rect id="svg-rect" x="50" y="50" width="50" height="50" fill="red" />' +
                '</svg>';
            $el = $(svgHtml);
            $(document.getElementById('test-fixture')).append($el);
            $rect = $('rect');

            $rect.tooltip({
                delayIn: 0,
                title: function () { return 'an svg element'; }
            });
        });

        afterEach(function () {
            clock.restore();
            $el.remove();
            $(document.body).find('.tipsy').remove();
        });

        it('will position correctly when attached to an svg element', function () {
            helpers.hover($rect);
            clock.tick(1);

            var $tipsies = visibleTipsies();
            var tipsyPosition = $tipsies.position();
            var rectPosition = $rect.position();
            var rectSize = $rect[0].getBoundingClientRect().width

            expect($tipsies.length).to.equal(1);
            expect(tipsyPosition.left).to.be.within(rectPosition.left - TIPSY_MARGIN, rectPosition.left + rectSize + TIPSY_MARGIN);
            expect(tipsyPosition.top).to.be.within(rectPosition.top + rectSize, rectPosition.top + rectSize + TIPSY_MARGIN);
        });
    });
});
