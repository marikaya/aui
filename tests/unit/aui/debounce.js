'use strict';

import debounce, { debounceImmediate } from '../../../src/js/aui/debounce';

describe('aui/debounce', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            debounce: debounce,
            debounceImmediate: debounceImmediate
        });
    });
});
